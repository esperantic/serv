var express = require('express');
var router = express.Router();

const users = [
    {
        "name": "0",
        "permissions": [
            "INCREMENT"
        ]
    },
    {
        "name": "1",
        "permissions": [
            "INCREMENT"
        ]
    },
    {
        "name": "2",
        "permissions": [
            "DECREMENT"
        ]
    },
    {
        "name": "3",
        "permissions": [
            "INCREMENT",
            "DECREMENT"
        ]
    },
    {
        "name": "4",
        "permissions": [
            "INCREMENT",
            "DECREMENT"
        ]
    }
];

/* GET users listing. */
router.get('/:id', function(req, res, next) {
  const id = req.params.id;
  res.json(users[id]);
});

module.exports = router;
